# derivatives.py
# example usage: derivatives.py -i iv7_VE628925.14_00.txt

import numpy as np

def calcFwdDiff(indata):
	# https://de.wikipedia.org/wiki/Numerische_Differentiation
	a=np.array([]) # contains result
	for i in range(len(indata[:,0])-1):
		a = np.append(\
			a, \
			(indata[i+1,2]-indata[i,2])/(indata[i+1,1]-indata[i,1]) \
		)
	a = np.append(a, np.nan) # last element can not be calculated.
	
	# assemble to internal array format
	b=[ indata[:,0],\
		indata[:,1],\
		a[:] ]
	c=np.array(b).transpose()
	return c


	
if __name__ == "__main__":
	print "Loading modules"

	import time
	perfstart = time.time()

	import ivparser as p
	#if p.version<0.91: exit("ivparser version insufficient")
	
	import argparse
	
	# set up argparse
	print "Parsing options"
	parser = argparse.ArgumentParser()
	parser.add_argument('-i', dest='infile', action='store', help='specify input file. Automatically sets TYPE according to prefix. Force a TYPE by using TYPE:filename, e.g.\'FULLNEEDLE:iv.dat\'. Supported formats: '+str(p.SETTINGS.keys()) )
	options = parser.parse_args()

	
	
	# open file
	ftype, fname = p.filestrParse(options.infile)
	infile = p.IVDataObject()
	infile.loadFile(fname, ftype)
	
	# create empty data types for holding d1, d2 data
	out_d1f = p.IVDataObject()
	out_d1f.parseFilename(infile.filename)
	temp_d1f = []
	
	out_d2f = p.IVDataObject()
	out_d2f.parseFilename(infile.filename)
	temp_d2f = []
	
	# calculate FwdDiff for each channel
	print "Calculating numeric forward differential"
	for ch in infile.channel_list:
		print '.',
		d0 = p.getChannelData(infile.data, ch)
		d1 = calcFwdDiff(d0)
		d2 = calcFwdDiff(d1)
		
		# concatenate all channels
		temp_d1f.append(d1)
		temp_d2f.append(d2)
	
	print '\n',
	# save to files
	infile.writeFile('ivp_'+ infile.shortFilename  +'_d0.txt')
	
	out_d1f.data = np.concatenate(temp_d1f)
	infile.writeFile('ivp_'+ out_d1f.shortFilename +'_d1.txt')
	
	out_d2f.data = np.concatenate(temp_d2f)
	infile.writeFile('ivp_'+ out_d2f.shortFilename +'_d2.txt')
	
	perfend = time.time()
	print "Time taken: "+str(perfend-perfstart)+"s"
