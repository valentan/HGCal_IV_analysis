# make_analysis.py
# create an automatic batch file

# TODO make this work in subfolders too
# TODO add a 'file exists' check, but this needs to be done in cmd?!

print "Loading modules"
import time
perfstart = time.time()
import os
import ivparser as p
#if p.version<0.91: exit("ivparser version insufficient")
from copy import copy

EMPTYLINEHELPER = 'echo\\\n'
ERRLVLHELPER = 'if not %errorlevel%==0 (\nchoice /c CA /m "ERROR detected. [C]ontinue or [A]bort?"\nif errorlevel==2 goto errorHandling\n)\n'+EMPTYLINEHELPER
ERRHANDLINGHELPER = 'goto done\n\n:errorHandling\necho an error ocurred, aborting batch process\n\n:done\necho End of batch script.\npause\n'
ANALYSISCUSTHELPER = 'if exist analysis_custom.bat (\necho running custom script\nanalysis_custom.bat\n)\n'

filelist=os.listdir('.') # list current directory

outfile = 'analysis_autopy.bat'
f = open(outfile,'w') # overwrites existing
# TODO check for write permission



outlines = [] # array to hold output
def addLine(text):
	global outlines
	#print text
	outlines.append(text+'\n')
	return
	
def checkErr():
	addLine(ERRLVLHELPER)
	

	
extensionlist = [ '.dat', '.dat.gz', '.txt', '.txt.gz' ]
# exclusionlist = [ '_tcorr.', '_rep.', 'reldev.txt' ]
exclusionlist = [ '_tcorr.', '_rep.', 'reldev.txt', '-r', '-t' ]
# TODO find a better(?) way to exclude split region/type files

# find all files with supported extension
templist = []
for x in filelist:
	passFlag=False
	for y in extensionlist:
		if y==x[-len(y)::]:
			templist.append(x)
			
# discard all files in exclusionlist
for x in copy(templist):
	for y in exclusionlist:
		if x.find(y)!=-1:
			try:
				templist.remove(x)
			except:
				pass # file already removed from list

filelist = copy(templist)

# open files for processing	
print "Making analysis_py.bat for files: "

farray = []
for x in filelist:
	temp = p.IVDataObject()
	temp.parseFilename( x )
	if temp.filetype is not None: # not None means filetype is recognized by ivparser
		print temp.filename
		farray.append(temp)

''' working order
type == IVPARSER:
	hexplot contact test
	hexplot current development
	plotAll single file
	page16plot single file
	page16plot all files
	custom analysis

type == FNTCORR:
	page16plot means and deviations *-reldev.pdf
	sanitize temperature data
	correct for temperature, variable mode
	hexplot contact test *-contact_test.pdf
	hexplot current development *-current_development.pdf
	plotAll single file *-ivall.pdf
	page16plot single file *-ivdetails.pdf
	page16plot all files *-comparison.pdf
	custom analysis 

type == FULLNEEDLE or SEVENNEEDLE:
	correct for temperature, constant mode
	hexplot contact test
	hexplot current development
	plotAll single file
	page16plot single file
	page16plot all files
	custom analysis 

'''

addLine('REM date of creation: '+time.strftime("%Y-%m-%d %H:%M") )
addLine('@echo off')

# create plotAlls and page16plot
for x in farray:
	addLine('echo #\necho # FILE: '+x.filename+'\necho #\n'+EMPTYLINEHELPER)
	
	if x.filetype=='IVPARSER':
		# no temperature correction
		pass
		
	elif x.filetype=='FNTCORR':
		# make and plot means and deviations - not temperature corrected
		addLine('echo ### making split rel.dev. files')
		addLine('ivt_split.py -m reldev -i %s' % x.filename)
		checkErr()

		addLine('echo ### plotting rel.dev.')
		addLine('plot16page.py '+\
			'-s 0 -df auto -batch -p 3,2 '+\
			'-xn "voltage:V" -xl \'0,-1000\' '+\
			'-yn "relative deviation:%%" -yl \'-2E-1,2E-1\' '+\
			'-y2n "reverse current:nA" -y2s log '+\
			'-e1 -ps \'-1E9\' -ps 1 -ps 1 -ps 1 -ps 1 -ps 1 '+\
			'-i ivp_%s_m0reldev.txt -l "IV curve" -i ivp_%s_m1reldev.txt -l "1st sample" -i ivp_%s_m2reldev.txt -l "2nd sample" -i ivp_%s_m3reldev.txt -l "3rd sample" -i ivp_%s_m4reldev.txt -l "4th sample" -i ivp_%s_m5reldev.txt -l "5th sample" -o %s-reldev.pdf -t "relative deviations, %s"' % ( x.shortFilename, x.shortFilename, x.shortFilename, x.shortFilename, x.shortFilename, x.shortFilename, x.shortFilename, x.shortFilename )\
			)
		checkErr()
		
		addLine('del ivp_%s_m*' % x.shortFilename)
		checkErr()
				
		# sanitize temps
		addLine('echo ### sanitizing temp values')
		addLine('sanitize_temps.py -i %s -o ivt_%s_rep.dat' % (x.filename, x.shortFilename) )
		checkErr()
		
		# plot temperature graph from raw data ? (py file not yet created)

		# create temp.corr. file - variable
		addLine('echo ### correcting for temperature difference')
		addLine('temp_corr.py -i ivt_%s_rep.dat -ft %s -tt %s -o ivp_%s_tcorr.txt' % (x.shortFilename, str('var'), str('22'), x.shortFilename ) )
		checkErr()
		
		addLine('del ivt_%s_rep.dat' % x.shortFilename)
		checkErr()
		
		x.filename='ivp_'+x.shortFilename+'_tcorr.txt'
		x.filetype='IVPARSER'
		
	elif x.filetype=='SEVENNEEDLE':
		# create temp.corr. file - constant
		addLine('echo ### correcting for temperature difference')
		addLine('temp_corr.py -i %s -ft %s -tt %s -o ivp_%s_tcorr.txt' % ( x.filename, str('19.3'), str('22'), x.shortFilename ) )
		checkErr()
		
		x.filename='ivp_'+x.shortFilename+'_tcorr.txt'
		x.filetype='IVPARSER'
	
	elif x.filetype=='FULLNEEDLE':
		# create temp.corr. file - constant
		addLine('echo ### correcting for temperature difference')
		addLine('temp_corr.py -i %s -ft %s -tt %s -o ivp_%s_tcorr.txt' % ( x.filename, str('27'), str('22'), x.shortFilename ) )
		checkErr()
		
		x.filename='ivp_'+x.shortFilename+'_tcorr.txt'
		x.filetype='IVPARSER'
		
		
	# make hexplot contact test
	addLine('echo ### MAKING HexPlot contact_test')
	addLine('plotHex.py -nowarn -i %s -m contact_test -o %s-contact_test.pdf' % ( x.filename, x.shortFilename ) )
	checkErr()
	
	# make hexplot current development
	addLine('echo ### MAKING HexPlot current_development')
	addLine('plotHex.py -nowarn -i %s -m current_development -o %s-current_development.pdf' % ( x.filename, x.shortFilename ) )
	checkErr()
		
	# make plotAll for single files, by type and region
	addLine('echo ### making split files, by type')
	addLine('quadrant_split.py -m type -g "Full;Small edge;Large edge;Mousebite;Small calibration,Large calibration;Guard" -i %s' % x.filename )
	checkErr()
	
	addLine('echo ### making split files, by region')
	addLine('quadrant_split.py -m region -0 -g "21;40;60;80" -i ivp_%s-t00.txt' % x.shortFilename)
	checkErr()
	
	# addLine('echo ### MAKING plotAll (single)')
	# addLine('plotAll.py '+\
		# '-batch -df c '+\
		# '-xn "voltage:V" -xl \'0,-1000\' '+\
		# '-yn "reverse current:nA" -ys log -yl 1E-1,1E4 -ps \'-1E9\' '+\
		# '-i ivp_%s-t00-r00.txt -l "Full 21u" -i ivp_%s-t00-r01.txt -l "Full 40u" -i ivp_%s-t00-r02.txt -l "Full 60u" -i ivp_%s-t00-r03.txt -l "Full 80u" -i ivp_%s-t00-r04.txt -l "Full other" -i ivp_%s-t01.txt -l "Small edge" -i ivp_%s-t02.txt -l "Large edge" -i ivp_%s-t03.txt -l "Mousebite" -i ivp_%s-t04.txt -l "Calibration" -i ivp_%s-t05.txt -l "Guard" -t "%s" -o %s-ivall.pdf' % ( x.shortFilename, x.shortFilename, x.shortFilename, x.shortFilename, x.shortFilename, x.shortFilename, x.shortFilename, x.shortFilename, x.shortFilename, x.shortFilename, x.shortFilename, x.shortFilename) \
		# )
	# checkErr()
	
	
	
	addLine('echo ### MAKING plotAll multipage, by type and region')
	temp = [ 
		[ 'Full 21u', 'ivp_%s-t00-r00.txt' % x.shortFilename ],
		[ 'Full 40u', 'ivp_%s-t00-r01.txt' % x.shortFilename ],
		[ 'Full 60u', 'ivp_%s-t00-r02.txt' % x.shortFilename ],
		[ 'Full 80u', 'ivp_%s-t00-r03.txt' % x.shortFilename ],
		[ 'Full other', 'ivp_%s-t00-r04.txt' % x.shortFilename ],
		[ 'Small edge', 'ivp_%s-t01.txt' % x.shortFilename ],
		[ 'Large edge', 'ivp_%s-t02.txt' % x.shortFilename ],
		[ 'Mousebite', 'ivp_%s-t03.txt' % x.shortFilename ],
		[ 'Calibration', 'ivp_%s-t04.txt' % x.shortFilename ],
		[ 'Guard', 'ivp_%s-t05.txt' % x.shortFilename ]
	]
		
	tempfilestr=''
	for m,n in enumerate(temp):
		addLine('plotAll.py '+\
			'-batch -df auto '+\
			'-xn "voltage:V" -xl \'0,-1000\' '+\
			'-yn "reverse current:nA" -ys log -yl 1E-1,1E4 -ps \'-1E9\' '+\
			'-c1 %i -i %s  -t "%s: %s" -o tmp2_%02i.pdf' % ( m, n[1], x.shortFilename, n[0], (m+1) ) \
			)
		checkErr()
		tempfilestr+='-i %s -l "%s" ' % ( n[1], n[0] )
	
	addLine('plotAll.py '+\
		'-batch -df cl '+\
		'-xn "voltage:V" -xl \'0,-1000\' '+\
		'-yn "reverse current:nA" -ys log -yl 1E-1,1E4 -ps \'-1E9\' '+\
		tempfilestr +\
		'-t "%s" -o tmp2_00.pdf' % x.shortFilename \
		)
	checkErr()
	
	# concatenate tmp_*.pdf into one file
	addLine('pdftk tmp2_* cat output %s-ivall.pdf' % x.shortFilename)
	checkErr()
	
	# cleanup temporary files
	addLine('del tmp2_*')
	checkErr()
	
	addLine('del ivp_%s-t*' % x.shortFilename)
	checkErr()
	
	# make plot16page for single files
	addLine('echo ### MAKING plot16page (single)')
	addLine('plot16page.py '+\
		'-df auto -batch '+\
		'-xn "voltage:V" -xl \'0,-1000\' '+\
		'-yn "reverse current:nA" -ys log -ps \'-1E9\' '+\
		'-i %s -t "IV details, %s" -o %s-ivdetails.pdf' % (x.filename, x.shortFilename, x.shortFilename) \
		)
	checkErr()
	
	# testing: test_500v
	addLine('echo ### TESTING test_500V')
	addLine('test_current_at_voltage.py -p -nv 500 -lim 8 -i %s' % x.filename )
	checkErr()

# make plot16page for all files in directory (FNTCORR only corrected data)
inputfilestr=""
for x in farray:
	inputfilestr += ' -i %s -l %s' % ( x.filename, x.shortFilename )
addLine('echo ### MAKING plot16page (all files)')
addLine('plot16page.py '+\
	'-df auto -batch '+\
	'-xn "voltage:V" -xl \'0,-1000\' '+\
	'-yn "reverse current:nA" -ys log -ps \'-1E9\' '+\
	'%s -t "compare all, %s" -o %s-comparison.pdf' % (inputfilestr, x.sensorID, x.sensorID) \
	)
checkErr()
	
# run diagnostics and testing suite for all single files (py file not yet created)

# add a command to start a custom batch file 'analysis_custom.bat'
addLine(ANALYSISCUSTHELPER)

addLine(ERRHANDLINGHELPER)

print "Writing file "+outfile
f.writelines(outlines)
f.close()

perfend = time.time()
print "Time taken: "+str(perfend-perfstart)+"s"


os.system('pause')
	
