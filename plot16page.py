# plot16page.py
# example usage: plot16page.py -s 0 -df auto -batch -p 3,2 -xn "voltage:V" -xl '0,-1000' -yn "relative deviation:%%" -yl '-2E-1,2E-1' -y2n "reverse current:nA" -y2s log -e1 -ps '-1E9' -ps 1 -ps 1 -ps 1 -ps 1 -ps 1 -i ivp_VE628925.11_00_m0reldev.txt -l "IV curve" -i ivp_VE628925.11_00_m1reldev.txt -l "1st sample" -i ivp_VE628925.11_00_m2reldev.txt -l "2nd sample" -i ivp_VE628925.11_00_m3reldev.txt -l "3rd sample" -i ivp_VE628925.11_00_m4reldev.txt -l "4th sample" -i ivp_VE628925.11_00_m5reldev.txt -l "5th sample" -o VE628925.11_00-reldev.pdf -t "relative deviations, VE628925.11_00"
# example usage: plot16page.py -df auto -batch -xn "voltage:V" -xl '0,-1000' -yn "reverse current:nA" -ys log -ps '-1E9' -i iv7_VE628925.14_70.txt -t "IV details, VE628925.11_00" -o VE628925.14_70-ivdetails.pdf

print "Loading modules"

import time
perfstart = time.time()

import ivparser as p
#if p.version<0.91: exit("ivparser version insufficient")

import numpy as np

import matplotlib.pyplot as plt 
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.ticker as mtick

from os import system

import argparse
# https://docs.python.org/3/library/argparse.html
# https://docs.python.org/2/howto/argparse.html
# optparse: https://docs.python.org/2.4/lib/optparse-creating-parser.html



# constants
scale_negNanoAmps = -1E9 # set scale to nA, reverse current
scale_negPicoAmps = -1E12
paper_A4 = (297/25.4,210/25.4) # size of A4 paper in inches (landscape)

plotSettings_default={'linewidth':'0.5'}
marker_list = ['x','.','v','^','s'] # every input file gets a different marker, recycling if > len(marker_list)
color_list = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']

ll = 2
ldot = ll*1
ldash = ll*5
lspace = ll*2
linestyle_list = [ # custom linestyle list
	(0, (ldash,lspace, ldot,lspace ) ),
	(0, (ldash,lspace, ldot,lspace, ldot,lspace ) ),
	(0, (ldash,lspace, ldot,lspace, ldot,lspace, ldot,lspace ) ),
	(0, (ldash,lspace, ldot,lspace, ldot,lspace, ldot,lspace, ldot,lspace ) ),
	
	(0, (ldash,lspace, ldash,lspace, ldot,lspace ) ),
	(0, (ldash,lspace, ldash,lspace, ldot,lspace, ldot,lspace ) ),
	(0, (ldash,lspace, ldash,lspace, ldot,lspace, ldot,lspace, ldot,lspace ) ),
	(0, (ldash,lspace, ldash,lspace, ldot,lspace, ldot,lspace, ldot,lspace, ldot,lspace ) ),
	
	(0, (ldash,lspace, ldash,lspace, ldash,lspace, ldot,lspace ) ),
	(0, (ldash,lspace, ldash,lspace, ldash,lspace, ldot,lspace, ldot,lspace ) ),
	(0, (ldash,lspace, ldash,lspace, ldash,lspace, ldot,lspace, ldot,lspace, ldot,lspace ) ),
	(0, (ldash,lspace, ldash,lspace, ldash,lspace, ldot,lspace, ldot,lspace, ldot,lspace, ldot,lspace ) )
]



# set up argparse
print "Parsing options"
parser = argparse.ArgumentParser()
parser.add_argument('-o', dest='outfile', default='output.pdf', help='specify output filename')
parser.add_argument('-i', dest='infile', action='append', help='Multiple input files can be specified. Automatically sets TYPE according to prefix. Force a TYPE by using TYPE:filename, e.g.\'FULLNEEDLE:iv.dat\'. Supported formats: '+str(p.SETTINGS.keys()) )
parser.add_argument('-c', dest='channel', action='store', help='Which channels to plot. Separate multiple channels with a comma \',\' or select ranges with \'-\'. If omitted, all channels will be selected.')
parser.add_argument('-df', dest='dfiles', default='auto', help='m,c,l Choose how to differentiate files: m Markers, c Colors, l Linestyle, auto MatPlotLib standard. Any combination of m,c,l possible.')
parser.add_argument('-t', dest='title', default='', help='Set the plot title, e.g. "whitespace allowed"')
parser.add_argument('-l', dest='labels', action='append', help='set one label per input file, e.g. "whitespace allowed"' )
parser.add_argument('-ps', dest='prescale', action='append', help='Set additional value scale, e.g. for switching data from A to nA with \'-ps 1e9\' (default: 1). Specify either once per file to set different scales, or only once to apply the same scale to all files.')

parser.add_argument('-batch', action='store_true', help='Batch mode - does not open PDF when finished.')
parser.add_argument('-s', action='store', default=None, help='Which files to plot on secondary axis. Select multiple with a comma \',\' or select ranges with \'-\'. If omitted, only the primary axis will be used.')

parser.add_argument('-p', dest='xyplots', default='4,4', help='amount of plots per page x,y , e.g. \'3,2\'')
parser.add_argument('-xn', dest='xname', help='set x axis label, e.g. "voltage:V"')
parser.add_argument('-yn', dest='yname', help='set y axis label, e.g. "reverse current:nA"')
parser.add_argument('-y2n', dest='y2name', help='set secondary y axis label, e.g. "relative deviation:%%"')

parser.add_argument('-xl', dest='xlim', help='set x limits, e.g. \'-1000,0\'')
parser.add_argument('-yl', dest='ylim', help='set y limits, e.g. \'0,4E3\'')
parser.add_argument('-y2l', dest='y2lim', help='set secondary y limits, e.g. \'-1,1\'')

parser.add_argument('-xs', dest='xscale', default='linear', help='set x scale (default: linear)')
parser.add_argument('-ys', dest='yscale', default='linear', help='set y scale (default: linear)')
parser.add_argument('-y2s', dest='y2scale', default='linear', help='set secondary y scale (default: linear)')

parser.add_argument('-e1', action='store_true', help='[special] emphasise first file' )

options = parser.parse_args()



outfile = options.outfile
plot_title = options.title

user_xscale  = options.xscale
user_yscale  = options.yscale
user_y2scale = options.y2scale

user_xmin, user_xmax   = p.settingsPairParseFloat(options.xlim)
user_ymin, user_ymax   = p.settingsPairParseFloat(options.ylim)
user_y2min, user_y2max = p.settingsPairParseFloat(options.y2lim)

xplots, yplots = p.settingsPairParseString(options.xyplots)
xplots, yplots = int(xplots), int(yplots)
sumplots = xplots*yplots

user_xname, user_xunit   = p.settingsPairParseString(options.xname, ':')
user_yname, user_yunit   = p.settingsPairParseString(options.yname, ':')
user_y2name, user_y2unit = p.settingsPairParseString(options.y2name, ':')

if options.labels is not None and len(options.labels)!=len(options.infile):
	print "ERROR -l please specify one label per input file"
	raise Exception
		
infile = [] # input files array
global_channel_list = [] # array for collecting all channels

for j,k in enumerate(options.infile):
	# open file
	ftype, fname = p.filestrParse(k)
	temp = p.IVDataObject()
	temp.loadFile(fname, ftype)
	infile.append(temp)
	
	if options.labels is not None:
		infile[j].plotSettings['label']=options.labels[j].strip('"')
	
	# set a fixed marker for each file
	if options.dfiles=='auto':
		# infile[j].plotSettings['marker'] = ''
		# infile[j].plotSettings['color'] = ''
		# infile[j].plotSettings['linestyle'] = ''
		pass
	else:
		if options.dfiles.find('m')!=-1:
			infile[j].plotSettings['marker'] = marker_list[ j%len(marker_list) ]
		if options.dfiles.find('c')!=-1:
			infile[j].plotSettings['color'] = color_list[ j%len(color_list) ]
		if options.dfiles.find('l')!=-1:
			infile[j].plotSettings['linestyle'] = linestyle_list[ j%len(linestyle_list) ]
		
if options.prescale is None:
	for f in infile:
		f.plot_prescale = 1
elif len(options.prescale)==1:
	for f in infile:
		f.plot_prescale=float( options.prescale[0].strip('\'') )
elif len(options.prescale)==len(options.infile):
	for i,f in enumerate(infile):
		f.plot_prescale=float( options.prescale[i].strip('\'') )
else:
	print "ERROR -ps please specify either one value or one value per input file"
	raise Exception
				
if options.channel is None: 
	# collect channels from all files
	for k in infile:
		global_channel_list = np.concatenate( ( global_channel_list,k.getChannels() ) )
	global_channel_list = np.unique(global_channel_list)
else: 
	global_channel_list = p.chParse(options.channel)
	
if options.batch:
	batchmode = True
else: 
	batchmode = False
	
if options.s is not None:
	useSecondary = True # set a global flag
	
	secondary_list = p.chParse(options.s)
	for i,f in enumerate(infile):
		f.secAxProp = False
		for j in secondary_list:
			if i==j:
				f.secAxProp = True
				print "Plotting "+f.filename +" on secondary axis"
else:
	useSecondary = False

if options.e1==True:
	print "[-e1] Emphasising "+infile[0].filename
	infile[0].plotSettings['linewidth']=1.5


	
pp = PdfPages(outfile) # open multi-page pdf

print "Generating plots for channels: "
pagecount = global_channel_list[0::sumplots].size

for k in range(0,global_channel_list.size, sumplots): # page/figure loop
	print str(global_channel_list[k:k+sumplots])
	
	# plot a multi-graph comparison in one figure
	fig, axarr=plt.subplots(xplots, yplots, figsize=paper_A4) 
	axarr = axarr.flatten() # makes axarr 1D so we can iterate
	
	if useSecondary:
		axarr2=[] # make secondary axes array
		for i in axarr:
			axarr2.append(i.twinx())
	
	for j in range(0, global_channel_list[k:k+sumplots].size): # subplot loop
		
		for l in infile: # files loop
			if useSecondary and l.secAxProp:
				targetAx=axarr2[j]
			else:
				targetAx=axarr[j]
			
			chData = l.getChannelData(global_channel_list[k+j])
			p.axPlotChannel( targetAx, chData, p.merge_dicts(plotSettings_default, l.plotSettings), l.plot_prescale )
				
		# plot settings 
		# http://matplotlib.org/api/axes_api.html
		axarr[j].set_xscale(user_xscale)
		axarr[j].set_yscale(user_yscale)
		axarr[j].set_xlim(left=user_xmin, right=user_xmax)
		axarr[j].set_ylim(bottom=user_ymin, top=user_ymax)
		axarr[j].grid(axis='x', which='major', linestyle='dashed', linewidth=0.5)
		axarr[j].grid(axis='x', which='minor', linestyle='dotted', linewidth=0.25)
		axarr[j].minorticks_on()
		title = 'ch'+str( int(global_channel_list[j+k]) )
		axarr[j].set_title( title, {'fontsize':8}, loc='left' )
		axarr[j].tick_params(axis='both', which='major', labelsize=8)
		axarr[j].yaxis.set_major_formatter( mtick.FormatStrFormatter('%.1e') )
		
		if useSecondary:
			axarr2[j].set_yscale(user_y2scale)
			axarr2[j].set_ylim(bottom=user_y2min, top=user_y2max)
			axarr2[j].grid(axis='y', which='major', linestyle='dashed', linewidth=0.5) # prefer y grid on secondary axis
			axarr2[j].grid(axis='y', which='minor', linestyle='dotted', linewidth=0.25)
			axarr2[j].minorticks_on()
			axarr2[j].tick_params(axis='both', which='major', labelsize=8)
			#axarr2[j].yaxis.set_major_formatter( mtick.FormatStrFormatter('%.1e') )
		else: 
			axarr[j].grid(axis='y', which='major', linestyle='dashed', linewidth=0.5)
			axarr[j].grid(axis='y', which='minor', linestyle='dotted', linewidth=0.25)
			
	
	# turns off empty plots at the end of the file
	for j in range(j+1,sumplots):
		axarr[j].axis('off') 
		if useSecondary:
			axarr2[j].axis('off')
	
	# figure settings
	print "Making layout"
	fig.text(1-0.5, 1-0.04, plot_title, ha='center', fontsize=12)
	fig.text(0.5, 0.04, user_xname+' ['+user_xunit+']', ha='center')
	fig.text(0.04, 0.5, user_yname+' ['+user_yunit+']', va='center', rotation='vertical')
	if useSecondary:
		fig.text(1-0.04, 1-0.5, user_y2name+' ['+user_y2unit+']', va='center', rotation='vertical')
	plt.figlegend()
	plt.tight_layout()
	if not useSecondary:
		plt.subplots_adjust(left=0.11, right=1-0.03, top=1-0.07, bottom=0.09) # paper_A4
	else: 
		plt.subplots_adjust(left=0.11, right=1-0.10, top=1-0.07, bottom=0.09) # paper_A4
	
	# save page to PDF
	print "Saving page"
	plt.savefig(pp, format='pdf')


	
print "Generated "+str(pagecount)+" pages"
print "Saving PDF"
pp.close()
perfend = time.time()
print "Time taken: "+str(perfend-perfstart)+"s"

if not batchmode:
	system(outfile) # opens file in default application

exit()
