# plotHex.py
# example usage: plotHex.py -i IVPARSER:ivp_VE641657.09_00.txt -m contact_test -o VE641657.09_00-contact_test.pdf

print "Loading modules"

import time
perfstart = time.time()

import ivparser as p
#if p.version<0.91: exit("ivparser version insufficient")

from os import system
from os import path
cDir=path.dirname(path.realpath(__file__)) # directory where this script is located (!= working directory)

import argparse



def execAndErr(command):
	if system(command)!=0:
		raise Exception
	else:
		return 0


		
# set up argparse
print "Parsing options"
parser = argparse.ArgumentParser()
parser.add_argument('-o', dest='outfile', help='specify output filename')
parser.add_argument('-i', dest='infile', action='store', help='specify input file. Automatically sets TYPE according to prefix. Force a TYPE by using TYPE:filename, e.g.\'FULLNEEDLE:iv.dat\'. Supported formats: '+str(p.SETTINGS.keys()) )
parser.add_argument('-m', dest='workmode', action='store', help='working mode: contact_test or current_development')
parser.add_argument('-nowarn', dest='nowarn', action='store_true', help='disable warnings and only end process gracefully')
options = parser.parse_args()

# open file
ftype, fname = p.filestrParse(options.infile)
infile = p.IVDataObject()
infile.loadFile(fname, ftype)


outfile = options.outfile
geofile = cDir+'\hex_positions_IFX_256ch_8inch.txt'
# TODO check if geofile exists!



# determine hexplotformat based on filetype
infile.hexplotformat = ''
if infile.filetype=='FULLNEEDLE' or infile.filetype=='FNTCORR':
	infile.hexplotformat = 'SELECTOR:no:PADNUM:VAL:no:no'
elif infile.filetype=='IVPARSER':
	infile.hexplotformat = 'PADNUM:SELECTOR:VAL'
elif infile.filetype=='SEVENNEEDLE':
	infile.hexplotformat = 'SELECTOR:PADNUM:VAL'

# create a reverse sorted voltage list, starting at U=0V
infile.voltageList = [val for val in p.getVoltages(infile.data)[::-1] if val<=0]

if len(infile.voltageList)>100:
		print "WARNING Cannot process such large files, this is a limitation of HexPlot.exe"
		if options.nowarn is None:
			raise Exception
		else:
			exit(0)

if infile.hexplotformat == '':
	print "hexplotformat not set"
	raise Exception

if options.workmode == 'contact_test':
	if options.outfile == None:
		outfile = infile.shortFilename+'-contact_test.pdf'
	
	# check if +1V data exists before continuing
	dataPresentFlag = False
	for v in p.getVoltages(infile.data)[::-1]:
		if v==1:
			dataPresentFlag = True
	if not dataPresentFlag:
		print "WARNING can't plot contact_test, no data available"
		if options.nowarn is None:
			raise Exception
		else:
			exit(0)
	
	# make hexplot contact test
	print '\nMaking HexPlot contact test (+1V)'
	execAndErr('HexPlot.exe -g "%s" -o %s --select 1 --nd 1 --pn 0 --ys 1e9 --vn current:I:nA -i %s --if %s -z 0:50 --info tr:%s' % (geofile, outfile, infile.filename, infile.hexplotformat, infile.shortFilename ) )
	
elif options.workmode == 'current_development':
	if options.outfile == None:
		outfile = infile.shortFilename+'-current_development.pdf'
	
	else:
		# make hexplot current development
		print "Found "+str(len(infile.voltageList))+" distinct voltages to plot"
		print '\nMaking HexPlot current development'
		
		for num in infile.voltageList:
			#print '%04i' % -num # reversed list of voltages
			execAndErr('HexPlot.exe -g "%s" -o tmp_%04i.pdf --select %i --nd 2 --pn 0 --ys -1e9 --vn current:I:nA -i %s --if %s -z -1:4000 --info tr:%s' % (geofile, -num, num, infile.filename, infile.hexplotformat, infile.shortFilename ) )
		
		# concatenate tmp_*.pdf into one file
		execAndErr('pdftk tmp_* cat output %s' % outfile)
		execAndErr('del tmp_*')


perfend = time.time()
print "Time taken: "+str(perfend-perfstart)+"s"
