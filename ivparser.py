# ivparser.py
# example usage: ivparser.py -i iv7_VE628925.14_00.txt -o ivp_VE628925.14_00.txt

import numpy as np
# https://docs.scipy.org/doc/numpy/reference/generated/numpy.loadtxt.html

import matplotlib.pyplot as plt 
# https://matplotlib.org/tutorials/introductory/usage.html#sphx-glr-tutorials-introductory-usage-py
# https://matplotlib.org/api/pyplot_summary.html
# https://matplotlib.org/devdocs/api/_as_gen/matplotlib.axes.Axes.set_title.html

from matplotlib.backends.backend_pdf import PdfPages
# https://matplotlib.org/faq/howto_faq.html#save-multiple-plots-to-one-pdf-file

import matplotlib.ticker as mtick
# https://stackoverflow.com/questions/25750170/can-i-show-decimal-places-and-scientific-notation-on-the-axis-of-a-matplotlib-pl

from os import system

#version = 0.91



typeassoc = [ 
	['iv_', 'FULLNEEDLE'],
	['ivp_', 'IVPARSER'],
	['iv7_', 'SEVENNEEDLE'],
	['ivt_', 'FNTCORR']
]





class IVDataObject:

	def __init__(self):
		
		self.data = []
		self.noData = True
		self.settings = None
		self.filename = None
		self.plotSettings = {}
		
		return
	
	def loadFile(self, filename, settingsObj=None):
			
		self.parseFilename(filename)
		
		if settingsObj is None: # automatic filetype recognition
			self.settings = SETTINGS[self.filetype]
		else:
			self.settings = settingsObj
				
		print "Loading file "+self.filename+" as "+self.filetype
		a=np.loadtxt(self.filename)
		
		if len(a)>0:
			b=[ a[:,self.settings.channel_column],\
				self.settings.voltage_prescale * a[:,self.settings.voltage_column],\
				self.settings.current_prescale * a[:,self.settings.current_column] ]
			c=np.array(b).transpose()
		
			self.rawData = a
			self.data = c
			self.noData = False
			self.channel_list = np.unique(self.data[:,0])
			
		else: # we tried to load the file, but it's empty
			self.rawData = None
			self.data = np.array( [[np.nan, np.nan, np.nan]] )
			self.noData = True
			self.channel_list = np.array([])
		
		return
		
	def parseFilename(self, filename):
		self.filename = filename
		
		try:
			# filename structure FILETYPE_SENSORID_MEASID.EXT
			self.filetype = None
			for y in typeassoc:
				if self.filename.find(y[0])==0:
					self.filetype = y[1]
			
			fsplit = self.filename.replace('\\', '/').split('/')[-1]
			self.sensorID      = fsplit.split('_')[1]
			self.measID        = fsplit.split('_')[2].split('.')[0]
			self.shortFilename = self.sensorID+'_'+self.measID
			# self.fileext       = fsplit.split('.')[-1]
			return 0
			
		except: 
			# filename does not adhere to standard structure
			self.filetype      = None
			self.sensorID      = None
			self.measID        = None
			self.shortFilename = None
			# self.fileext       = None
			return -1
		
	def writeFile(self, outfile): # exports an IVPARSER formatted file
		print "Exporting data to "+outfile
		with open(outfile, 'w') as f: # overwrites existing file
			f.write('# original file: '+self.filename+'\n')
			f.write('# config columns=channel,voltage,current\n')
			f.write('# \n')
			f.write('# Channel [-]\tVoltage [V]\tCurrent [A]\n')
			np.savetxt(f, self.data)
		return

	##### LEGACY FUNCTIONS
	def getChannels(self):
		return self.channel_list
		
	def plotChannel(self, channel, param_dict={}, prescale=1):
		return plotChannel(self.getChannelData(channel), param_dict, prescale)
		
	def getChannelData(self, channel):
		return getChannelData(self.data, channel)

	def axPlotChannel(self, ax, channel, param_dict={}, prescale=1):
		return axPlotChannel(ax, self.getChannelData(channel), param_dict, prescale)
		
		
		
	
	
class IVSettingsObject:

	voltage_prescale = 1
	current_prescale = 1

	def __init__(self, channel_column, voltage_column, current_column):
		self.channel_column = channel_column
		self.voltage_column = voltage_column
		self.current_column = current_column

		
		
FULLNEEDLE = IVSettingsObject(\
	channel_column=2, \
	voltage_column=0, \
	current_column=3 )
	
FNTCORR = FULLNEEDLE

SEVENNEEDLE = IVSettingsObject(\
	channel_column=1, \
	voltage_column=0, \
	current_column=2 )
SEVENNEEDLE.current_prescale = -1

IVPARSER = IVSettingsObject(\
	channel_column=0, \
	voltage_column=1, \
	current_column=2 )

SETTINGS = {\
	'FULLNEEDLE':FULLNEEDLE, \
	'SEVENNEEDLE':SEVENNEEDLE, \
	'IVPARSER':IVPARSER, \
	'FNTCORR':FNTCORR \
	}

	
	
	
	
#
# functions for handling data
#

def IVDOfromFile(filename, settings): # LEGACY constructor helper
	a = IVDataObject()
	a.loadFile(filename, settings)
	return a

def plotChannel(chData, param_dict={}, prescale=1):
	plt.plot(chData[:,1], prescale*chData[:,2], **param_dict)
	return
	
def axPlotChannel(ax, chData, param_dict={}, prescale=1):
	if chData.size==0: return # dataset does not exist
	ax.plot(chData[:,1], prescale*chData[:,2], **param_dict)
	return

def getChannels(indata):
	return np.unique(indata[:,0])
	
def getChannelData(indata, channel):
	return np.array( [val for val in indata if (val[0]==channel)] )
	
def getVoltages(indata):
	return np.unique(indata[:,1])
	
	
	
#
# helper functions
#

def merge_dicts(*dict_args):
    """
    Given any number of dicts, shallow copy and merge into a new dict,
    precedence goes to key value pairs in latter dicts.
	https://stackoverflow.com/questions/38987/how-to-merge-two-dictionaries-in-a-single-expression
    """
    result = {}
    for dictionary in dict_args:
        result.update(dictionary)
    return result

def chParse(input):
	# parse comma and range separators
	a = input.split(',')
	t = [] # (python array supports append)
	for b in a:
		c = b.split('-')
		if len(c)==1:
			t.append(int(c[0]))
		elif len(c)==2:
			d,e=c
			for f in range(int(d), int(e)+1):
				t.append(f)
		else:
			print "illegal argument"
			raise Exception
	return np.unique( np.array(t) )
		
def settingsPairParseFloat(input, delim=','):
	a, b = settingsPairParseString(input, delim)
	if a == '':
		a = None
	else:
		a = float(a)
		
	if b == '':
		b = None
	else:
		b = float(b)
	return a, b
	
def settingsPairParseString(input, delim=','):
	if input==None:
		return '', ''
		
	temp = input.strip('\'').split(delim)
	if len(temp)>1:
		a, b = temp[0], temp[1]
	else: # len(temp)==1
		a, b = temp[0], None
	return a, b
	
def filestrParse(input):
	k = input.split(':')
	
	if len(k)>1:
		if k[0]=='':
			k[0]=None
		else:
			k[0]=SETTINGS[k[0]]
		ftype, fname = k[0], k[1]
	else: # len(k)==1
		ftype, fname = None, k[0]
		
	return ftype, fname



#
# converter functionality - standalone
#

if __name__=="__main__":
	print "Loading modules"

	import time
	perfstart = time.time()
	from os import system
	import argparse
	
	# set up argparse
	print "Parsing options"
	parser = argparse.ArgumentParser()
	parser.add_argument('-o', dest='outfile', default='ivp_output.txt', help='specify output filename, will create an IVPARSER file')
	parser.add_argument('-i', dest='infile', action='store', help='specify input file. Automatically sets TYPE according to prefix. Force a TYPE by using TYPE:filename, e.g.\'FULLNEEDLE:iv.dat\'. Supported formats: '+str(SETTINGS.keys()) )
	options = parser.parse_args()

	outfile = options.outfile
	
	ftype, fname = filestrParse(options.infile)
	infile = IVDataObject()
	infile.loadFile(fname, ftype)
	
	infile.writeFile(outfile)
	
	perfend = time.time()
	print "Time taken: "+str(perfend-perfstart)+"s"
