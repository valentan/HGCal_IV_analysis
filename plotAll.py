# plotAll.py
# usage example: plotAll.py -df auto -ys log -xl '-1000,0' -xn voltage:V -yl 1E1,1E4 -yn "reverse current:nA" -t "VE641657.09_00" -i ivp_VE641657.09_00.txt -ps '-1E9' -o VE641657.09_00-ivall.pdf
# usage example: plotAll.py -batch -df c -xn "voltage:V" -xl '0,-1000' -yn "reverse current:nA" -ys log -yl 1E-1,1E4 -ps '-1E9' -i ivp_VE628925.11_00-t00-r00.txt -l "Full 21u" -i ivp_VE628925.11_00-t00-r01.txt -l "Full 40u" -i ivp_VE628925.11_00-t00-r02.txt -l "Full 60u" -i ivp_VE628925.11_00-t00-r03.txt -l "Full 80u" -i ivp_VE628925.11_00-t00-r04.txt -l "Full other" -i ivp_VE628925.11_00-t01.txt -l "Small edge" -i ivp_VE628925.11_00-t02.txt -l "Large edge" -i ivp_VE628925.11_00-t03.txt -l "Mousebite" -i ivp_VE628925.11_00-t04.txt -l "Calibration" -i ivp_VE628925.11_00-t05.txt -l "Guard" -t "VE628925.11_00" -o VE628925.11_00-ivall.pdf

print "Loading modules"

import time
perfstart = time.time()

import ivparser as p
#if p.version<0.91: exit("ivparser version insufficient")

import numpy as np

import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.ticker as mtick

from os import system

import argparse

from collections import OrderedDict



# constants
scale_negNanoAmps = -1E9 # set scale to nA, reverse current
paper_A4 = (297/25.4,210/25.4) # size of A4 paper in inches (landscape)
paper_A5 = (210/25.4,148/25.4) # size of A5 paper (landscape)
customSize = (180/25.4, 127/25.4)

plotSettings_default={'linewidth':0.5} # https://github.com/matplotlib/matplotlib/issues/8759
marker_list = ['x','.','v','^','s'] # every input file gets a different marker, recycling if > len(marker_list)
color_list = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']

ll = 2
ldot = ll*1
ldash = ll*5
lspace = ll*2
linestyle_list = [ # custom linestyle list
	(0, (ldash,lspace, ldot,lspace ) ),
	(0, (ldash,lspace, ldot,lspace, ldot,lspace ) ),
	(0, (ldash,lspace, ldot,lspace, ldot,lspace, ldot,lspace ) ),
	(0, (ldash,lspace, ldot,lspace, ldot,lspace, ldot,lspace, ldot,lspace ) ),

	(0, (ldash,lspace, ldash,lspace, ldot,lspace ) ),
	(0, (ldash,lspace, ldash,lspace, ldot,lspace, ldot,lspace ) ),
	(0, (ldash,lspace, ldash,lspace, ldot,lspace, ldot,lspace, ldot,lspace ) ),
	(0, (ldash,lspace, ldash,lspace, ldot,lspace, ldot,lspace, ldot,lspace, ldot,lspace ) ),

	(0, (ldash,lspace, ldash,lspace, ldash,lspace, ldot,lspace ) ),
	(0, (ldash,lspace, ldash,lspace, ldash,lspace, ldot,lspace, ldot,lspace ) ),
	(0, (ldash,lspace, ldash,lspace, ldash,lspace, ldot,lspace, ldot,lspace, ldot,lspace ) ),
	(0, (ldash,lspace, ldash,lspace, ldash,lspace, ldot,lspace, ldot,lspace, ldot,lspace, ldot,lspace ) )
]



# set up argparse
print "Parsing options"
parser = argparse.ArgumentParser()
parser.add_argument('-o', dest='outfile', default='output.pdf', help='specify output filename')
parser.add_argument('-i', dest='infile', action='append', help='Multiple input files can be specified. Automatically sets TYPE according to prefix. Force a TYPE by using TYPE:filename, e.g.\'FULLNEEDLE:iv.dat\'. Supported formats: '+str(p.SETTINGS.keys()) )
parser.add_argument('-c', dest='channel', action='append', help='Which channels to plot. Seperate multiple channels with a comma \',\' or select ranges with \'-\'. If omitted, all channels will be selected. Specify either once per file to select different channels, or only once to select the same channel in all files.')
parser.add_argument('-df', dest='dfiles', default='auto', help='m,c,l Choose how to differentiate files: m Markers, c Colors, l Linestyle, auto MatPlotLib standard. Any combination of m,c,l possible.')
parser.add_argument('-t', dest='title', default='', help='Set the plot title.')
parser.add_argument('-labels', action='store_true', help='Show labels per channel')
parser.add_argument('-l', dest='label_list', action='append', help='set one label per input file, e.g. "whitespace allowed". Works best with -df c|m' )

parser.add_argument('-ps', dest='prescale', action='append', help='Set additional value scale, e.g. for switching data from A to nA with \'-ps 1e9\' (default: 1). Specify either once per file to set different scales, or only once to apply the same scale to all files.')

parser.add_argument('-batch', action='store_true', help='Batch mode - does not open PDF when finished.')

parser.add_argument('-xn', dest='xname', default=':', help='set x axis label, e.g. "voltage:V"')
parser.add_argument('-yn', dest='yname', default=':', help='set y axis label, e.g. "reverse current:nA"')

parser.add_argument('-xl', dest='xlim', help='set x limits, e.g. \'-1000,0\'')
parser.add_argument('-yl', dest='ylim', help='set y limits, e.g. \'0,4E3\'')

parser.add_argument('-xs', dest='xscale', default='linear', help='set x scale (default: linear)')
parser.add_argument('-ys', dest='yscale', default='linear', help='set y scale (default: linear)')

parser.add_argument('-c1', dest='c1', help='[special] override color of first file')

options = parser.parse_args()



outfile = options.outfile
plot_title = options.title

user_xscale  = options.xscale
user_yscale  = options.yscale

user_xmin, user_xmax   = p.settingsPairParseFloat(options.xlim)
user_ymin, user_ymax   = p.settingsPairParseFloat(options.ylim)

user_xname, user_xunit   = p.settingsPairParseString(options.xname, ':')
user_yname, user_yunit   = p.settingsPairParseString(options.yname, ':')

if options.label_list is not None and len(options.label_list)!=len(options.infile):
	print "ERROR -l please specify one label per input file"
	raise Exception

infile = [] # input files array
for j,k in enumerate(options.infile):
	# open file
	ftype, fname = p.filestrParse(k)
	temp = p.IVDataObject()
	temp.loadFile(fname, ftype)
	infile.append(temp)

	if options.label_list is not None:
		infile[j].plotSettings['label']=options.label_list[j].strip('"')

	# set a fixed marker for each file
	if options.dfiles=='auto':
		# infile[j].plotSettings['marker'] = ''
		# infile[j].plotSettings['color'] = ''
		# infile[j].plotSettings['linestyle'] = ''
		pass
	else:
		print "Filename  = " + fname
		print "Label     = " + options.label_list[j]
		if options.dfiles.find('m')!=-1:
			infile[j].plotSettings['marker'] = marker_list[ j%len(marker_list) ]
			print "Marker    = " + marker_list[ j%len(marker_list) ]
		if options.dfiles.find('c')!=-1:
			infile[j].plotSettings['color'] = color_list[ j%len(color_list) ]
			print "Color     = " + color_list[ j%len(color_list) ]
		if options.dfiles.find('l')!=-1:
			infile[j].plotSettings['linestyle'] = linestyle_list[ j%len(linestyle_list) ]
			#print "Linestyle = " + linestyle_list[ j%len(linestyle_list) ]




if options.c1 is not None:
	print "[-c1] override color of "+infile[0].filename
	infile[0].plotSettings['color'] = color_list[ int(options.c1)%len(color_list) ]

if options.prescale is None:
	for f in infile:
		f.plot_prescale = 1
elif len(options.prescale)==1:
	for f in infile:
		f.plot_prescale=float( options.prescale[0].strip('\'') )
elif len(options.prescale)==len(options.infile):
	for i,f in enumerate(infile):
		f.plot_prescale=float( options.prescale[i].strip('\'') )
else:
	print "ERROR -ps please specify either one value or one value per input file"
	raise Exception

if options.channel is None: # use all channels
	pass
elif len(options.channel) == 1: # use the same channels for all files
	for k in infile:
		k.channel_list = p.chParse(options.channel[0])
else: # assuming one channel per file has been specified
	for j,k in enumerate(infile):
		k.channel_list = p.chParse(options.channel[j])

if options.labels or options.label_list is not None:
	labels = True
else:
	labels = False

if options.batch:
	batchmode = True
else:
	batchmode = False



fig, ax=plt.subplots(1, 1, figsize=customSize)

for j,k in enumerate(infile):

	print "Generating plot from file "+k.filename+" with "+str(k.channel_list.size)+" channels."

	for ch in k.channel_list:
		# only put one curve per file in legend
		#if ch!=k.channel_list[0]:
		#	plotSettings_perChannel={'label':"_nolegend_"}
		#else:
		plotSettings_perChannel={}

		chData = k.getChannelData(ch)
		#plotSettings_perChannel={'label':k.shortFilename+'-ch'+str(ch)}
		p.axPlotChannel( ax, chData, p.merge_dicts(k.plotSettings, plotSettings_perChannel, plotSettings_default), k.plot_prescale)

# plot settings
# http://matplotlib.org/api/axes_api.html
ax.set_xscale(user_xscale)
ax.set_yscale(user_yscale)
ax.set_xlim(left=user_xmin, right=user_xmax)
ax.set_ylim(bottom=user_ymin, top=user_ymax)

ax.grid(axis='both', linestyle='dashed')
ax.minorticks_on()
ax.tick_params(axis='both', which='major', labelsize=8)
ax.yaxis.set_major_formatter( mtick.FormatStrFormatter('%.1e') )

# figure settings
fig.text(1-0.5, 1-0.04, plot_title, ha='center')
fig.text(0.5, 0.04, user_xname+' ['+user_xunit+']', ha='center')
fig.text(0.04, 0.5, user_yname+' ['+user_yunit+']', va='center', rotation='vertical')

if labels:
	handles, labels = plt.gca().get_legend_handles_labels()
	by_label = OrderedDict(zip(labels, handles)) # remove duplicate legend entries
	plt.legend(by_label.values(), by_label.keys(), handlelength=4, loc='lower right')
	#plt.figlegend(handlelength=4, loc='lower right')
plt.tight_layout()
# plt.subplots_adjust(left=0.11, right=0.97, top=0.93, bottom=0.09) # paper_A4
plt.subplots_adjust(left=0.15, right=1-0.03, top=1-0.07, bottom=0.10) # customSize

print "Saving PDF "+outfile
pp = PdfPages(outfile)
plt.savefig(pp, format='pdf')
pp.close()
perfend = time.time()
print "Time taken: "+str(perfend-perfstart)+"s"

if not batchmode:
	system(outfile) # opens file in default application

exit()
