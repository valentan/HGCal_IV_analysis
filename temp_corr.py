# temp_corr.py
# example usage: temp_corr.py -i FULLNEEDLE:ivt_VE628925.11_00_rep.dat -ft var -tt 22 -o ivp_VE628925.11_00_tcorr.txt

import ivparser as p
from math import pow, exp



def CtoK(temp):
	return temp+273.15
	
def tCorr(cRaw, t1, t2):
	# formula by Axel Koenig (?)
	# temperatures in Kelvin
	kB   = 8.617332478E-5 # eV K^-1
	Eeff = 1.21 # eV
	return pow(t2/t1, 2) * exp( -Eeff/(2*kB) * (1/t2-1/t1) ) * cRaw
	
	
	
if __name__=="__main__":

	print "Loading modules"
	import time
	perfstart = time.time()
	import argparse
	
	# set up argparse
	print "Parsing options"
	parser = argparse.ArgumentParser()
	parser.add_argument('-i', dest='infile', action='store', help='specify input file. Automatically sets TYPE according to prefix. Force a TYPE by using TYPE:filename, e.g.\'FULLNEEDLE:iv.dat\'. Supported formats: '+str(p.SETTINGS.keys()) )
	parser.add_argument('-ft', dest='fromtemp', help='temperature of input data, Celsius. Use \'var\' for variable temperature on FNTCORR files.')
	parser.add_argument('-o', dest='outfile', default='ivp_output_tcorr.txt', help='specify output filename')
	parser.add_argument('-tt', dest='totemp', help='reference temperature, Celsius')
	options = parser.parse_args()

	
	
	outfile = options.outfile
	
	if options.totemp is None:
		print "-tt argument is mandatory"
		raise Exception
		
	# open file
	ftype, fname = p.filestrParse(options.infile)
	infile = p.IVDataObject()
	infile.loadFile(fname, ftype)

	if options.fromtemp == 'var': # variable temperature mode
		infile.varTempCorr = True
		infile.t1_K = []
	elif options.fromtemp is None and infile.filetype=='FNTCORR':
		infile.varTempCorr = True
		infile.t1_K = []
	else: # constant temperature mode
		print "Using constant temperature correction"
		infile.varTempCorr = False
		infile.t1_K = CtoK(float(options.fromtemp))
	infile.t2_K = CtoK(float(options.totemp))



	# correct data for temperature drift
	print "Working"
	if infile.varTempCorr:
		print "Using variable temperature correction from raw data, column 11"
		for i in infile.rawData[:,11]: # 11th column contains "Probecard Temp [deg.C]"
			infile.t1_K.append(CtoK(i))
		for i,j in enumerate(infile.data[:,2]):
			infile.data[i,2] = tCorr( infile.data[i,2], infile.t1_K[i], infile.t2_K )
	else:
		for i,j in enumerate(infile.data[:,2]):
			infile.data[i,2] = tCorr( infile.data[i,2], infile.t1_K, infile.t2_K )

	# save to file (type IVPARSER)
	infile.writeFile(outfile)
	
	perfend = time.time()
	print "Time taken: "+str(perfend-perfstart)+"s"
