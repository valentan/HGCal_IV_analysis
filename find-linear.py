# linearfit.py

print "Loading modules"

import time
perfstart = time.time()

import ivparser as p
#if p.version<0.4: exit("ivparser version insufficient")

import numpy as np

import matplotlib.pyplot as plt 
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.ticker as mtick

class IVDataObject(p.IVDataObject):
	def plotFunctionStub(self, channel, param_dict={}, prescale=1):
		pass
		return

#https://stackoverflow.com/questions/13691775/python-pinpointing-the-linear-part-of-a-slope


infile = IVDataObject("iv_VE641657.09_00.dat", p.FULLNEEDLE)



########
from matplotlib.mlab import stineman_interp

x = np.linspace(0,2*np.pi,20);
y = x + np.sin(x) + np.exp(-0.5*(x-2)**2);

num_points = len(x)

min_fit_length = 15

chi = 0

chi_min = 1

i_best = 0
j_best = 0

for i in range(len(x) - min_fit_length):
    for j in range(i+min_fit_length, len(x)):
		coefs = np.polyfit(x[i:j],y[i:j],1)
		y_linear = x * coefs[0] + coefs[1]
		chi = 0
		for k in range(i,j):
			chi += ( y_linear[k] - y[k])**2

		print chi

		if chi < chi_min:
			i_best = i
			j_best = j
			chi_min = chi
			print chi_min

coefs = np.polyfit(x[i_best:j_best],y[i_best:j_best],1)
y_linear = x[i_best:j_best] * coefs[0] + coefs[1]


fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(x,y,'ro')
ax.plot(x[i_best:j_best],y_linear,'b-')


plt.show()

