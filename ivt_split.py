# ivt_split.py
# relative deviation from mean for custom iv.dat (FULLNEEDLE) files from test02_scan_iv_custom.py
# example usage: ivt_split.py -i ivt_VE628925.11_00.dat -m split

''' columns for ivdat_custom files
0 Nominal Voltage [V]	 
1 Measured Voltage [V]	
2 Channel [-]	
3 Current [A]	
4 Current Error [A]	
5 Total Current[A]	
6 Meas-1 [A]	
7 Meas-2 [A]	
8 Meas-3 [A]	
9 Meas-4 [A]	
10 Meas-5 [A]	
11 Probecard Temp [deg.C]	
12 Switchcard Temp [deg.C]	
13 Probecard Humi [%]	
14 Switchcard Humi [%]
'''

def upperLimVoltage(indata, limit):
	return [val for val in indata if (val[1]<limit)]

def calcRelMeanDev(value, mean):
	return (value-mean)/mean*100

def calcRelMeanDevAll(valueObj, meanObj):
	for i,j in enumerate(valueObj.data[:,2]):
		valueObj.data[i,2] = calcRelMeanDev( valueObj.data[i,2], meanObj.data[i,2] )
	return	

	

if __name__=='__main__':
	print "Loading modules"

	import time
	perfstart = time.time()
	
	import ivparser as p

	import argparse
	# set up argparse
	print "Parsing options"
	parser = argparse.ArgumentParser()
	parser.add_argument('-i', dest='infile', action='store', help='input file, only test02_scan_iv_custom iv.dat supported')
	parser.add_argument('-m', dest='workmode', action='store', help='working mode: split or reldev')
	options = parser.parse_args()

	infile = options.infile

	m=[
		p.IVDOfromFile( infile, p.FULLNEEDLE ), # mean
		p.IVDOfromFile( infile, p.IVSettingsObject(2, 0,  6) ), # Meas-1
		p.IVDOfromFile( infile, p.IVSettingsObject(2, 0,  7) ), # Meas-2
		p.IVDOfromFile( infile, p.IVSettingsObject(2, 0,  8) ), # Meas-3
		p.IVDOfromFile( infile, p.IVSettingsObject(2, 0,  9) ), # Meas-4
		p.IVDOfromFile( infile, p.IVSettingsObject(2, 0, 10) )  # Meas-5
	]
	
	if options.workmode=='reldev':
		for i,j in enumerate(m):
			if i!=0:
				calcRelMeanDevAll(m[i],m[0])
				m[i].data = upperLimVoltage(m[i].data, 0) # ignore values at 0V and above
			j.writeFile('ivp_'+j.shortFilename+'_m'+str(i)+'reldev.txt')
			
	elif options.workmode=='split':
		for i,j in enumerate(m):
			j.writeFile('ivp_'+j.shortFilename+'_m'+str(i)+'.txt')

	else:
		print "ERROR -m invalid choice"
		raise Exception

	perfend = time.time()
	print "Time taken: "+str(perfend-perfstart)+"s"


