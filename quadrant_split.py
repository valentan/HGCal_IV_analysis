# quadrant_split.py
# split pad data into multiple files according to pad type and/or quadrant
# example usage: quadrant_split.py -m type -g "Full;Small edge;Large edge;Mousebite;Small calibration,Large calibration;Guard" -i ivp_VE628925.14_00.txt
# example usage: quadrant_split.py -m region -0 -g "21;40;60;80" -i ivp_VE628925.14_00-t00.txt

''' generated output files:
	00	full cell
		00	multi-u
		01	21u
		02	40u
		03	60u
		04 	80u
	01	small edge
	02	large edge 	
	03	mousebite
	04 	small & large calibration 
	05 	guard

'''

	

if __name__=='__main__':
	print "Loading modules"

	import time
	perfstart = time.time()
	
	import numpy as np
	
	import ivparser as p
	
	import ifxgeolib
	from ifxgeolib.ifxgeolib import elInArr
	ifx = ifxgeolib.IFX8in()
	
	import argparse
	# set up argparse
	print "Parsing options"
	parser = argparse.ArgumentParser()
	parser.add_argument('-i', dest='infile', action='store', help='input file, only test02_scan_iv_custom iv.dat supported')
	parser.add_argument('-m', dest='workmode', action='store', help='working mode: region or type')
	parser.add_argument('-g', dest='groups', action='store', help='categories as a list. "c1,c2;c3;c4,c5,c6" will create 3 files')
	parser.add_argument('-0', dest='defgrp', action='store_true', help='export an additional file of pads that don\'t fit in any group')
	# TODO implement "remaining groups" -0 feature
	parser.add_argument('-l', dest='listmode', action='store_true', help='show a list of regions and types in ifxgeolib')
	options = parser.parse_args()

	if options.listmode:
		print 'Available types: (-m type)'
		for i in ifx.type_list:
			print i
		print '\n'
		
		print 'Available regions: (-m region)'
		for i in ifx.region_list_str:
			print i
		print '\n'

		exit()
	

	
	# parse groups setting
	if options.groups is None:
		print "ERROR -g is mandatory"
		raise Exception
	else:
		groups = []
		for i in options.groups.split(';'):
			temp = []
			for j in i.split(','):
				if elInArr(j, ifx.type_list) or elInArr(j, ifx.region_list_str):
					temp.append(j)
				else:
					print 'ERROR -g: "'+j+'" unknown'
					raise Exception
			groups.append(temp)
		# print groups
	
	
	
	# open file
	ftype, fname = p.filestrParse(options.infile)
	infile = p.IVDataObject()
	infile.loadFile(fname, ftype)
	
	# read header from original file
	oheader = []
	with open(infile.filename, 'r') as orig:
		temp = orig.readlines(512*1024) # read the first 512 KB into an array
		for l in temp:
			if l[0]=='#':
				oheader.append('#'+l)
			else:
				break
	
	for i,g in enumerate(groups):
		if options.workmode == 'region':
			outfile = "ivp_"+infile.shortFilename+"-r%02i.txt" % i
		elif options.workmode == 'type':
			outfile = "ivp_"+infile.shortFilename+"-t%02i.txt" % i
			
		# create output files and write headers
		with open(outfile, 'w') as f: # overwrites existing file
			f.writelines(oheader)
			f.write('# \n')
			f.write('# original file: '+infile.filename+'\n')
			f.write('# config columns=channel,voltage,current'+'\n')
			f.write('# config group='+str(g)+'\n')
			f.write('# \n')
			f.write('# Channel [-]\tVoltage [V]\tCurrent [A]\n')	
	
		if options.defgrp == True:
			# create file for "remaining pads"
			if options.workmode == 'region':
				outfile = "ivp_"+infile.shortFilename+"-r%02i.txt" % (i+1)
			elif options.workmode == 'type':
				outfile = "ivp_"+infile.shortFilename+"-t%02i.txt" % (i+1)
				
			with open(outfile, 'w') as f: # overwrites existing file
				f.writelines(oheader)
				f.write('# \n')
				f.write('# original file: '+infile.filename+'\n')
				f.write('# config columns=channel,voltage,current'+'\n')
				f.write('# config group=defgrp\n')
				f.write('# \n')
				f.write('# Channel [-]\tVoltage [V]\tCurrent [A]\n')	
		
	
	
	if options.workmode=='region':

		remaining_pads = list(infile.channel_list)
		for i,g in enumerate(groups):
			outfile = "ivp_"+infile.shortFilename+"-r%02i.txt" % i
			print "Writing region "+str(g)+" to "+outfile+":",
			for h in g:
				with open(outfile, 'a') as f: # appends to existing file
					for ch in infile.channel_list:
						if ifx.byID(ch) is not None and ifx.byID(ch).regions==[h]:
							np.savetxt(f, p.getChannelData(infile.data, ch))
							remaining_pads.remove(ch)
			print "done"
			
		if options.defgrp==True:
			outfile = "ivp_"+infile.shortFilename+"-r%02i.txt" % (i+1)
			print "Writing remaining to "+outfile+":",
			with open(outfile, 'a') as f: # appends to existing file
				for ch in remaining_pads:
					if ifx.byID(ch) is not None and ifx.byID(ch).inRegion(h):
						np.savetxt(f, p.getChannelData(infile.data, ch))
			print "done"
		
	elif options.workmode=='type':
		
		remaining_pads = list(infile.channel_list)
		for i,g in enumerate(groups):
			outfile = "ivp_"+infile.shortFilename+"-t%02i.txt" % i
			print "Writing type "+str(g)+" to "+outfile+":",
			with open(outfile, 'a') as f: # appends to existing file
				for ch in infile.channel_list:
					if ifx.byID(ch) is not None and elInArr(ifx.byID(ch).type, g):
						np.savetxt(f, p.getChannelData(infile.data, ch))
						remaining_pads.remove(ch)
			print "done"
		
		if options.defgrp==True:
			outfile = "ivp_"+infile.shortFilename+"-t%02i.txt" % (i+1)
			print "Writing remaining to "+outfile+":",
			with open(outfile, 'a') as f: # appends to existing file
				for ch in remaining_pads:
					if ifx.byID(ch) is not None and elInArr(ifx.byID(ch).type, g):
						np.savetxt(f, p.getChannelData(infile.data, ch))
			print "done"
					
	else:
		print "ERROR -m invalid choice"
		raise Exception

		
	
	perfend = time.time()
	print "Time taken: "+str(perfend-perfstart)+"s"


