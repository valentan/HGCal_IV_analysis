# sanitize_temps.py
# example usage: sanitize_temps.py -i ivt_VE628925.11_00.dat -o ivt_VE628925.11_00_rep.dat

import ivparser as p
#if p.version<0.91: exit("ivparser version insufficient")

import numpy as np



def removeFalseTemps(indata):
	global a
	a = []
	for x in indata:
		if x==-1:
			a.append(np.nan)
		else:
			a.append(x)
							
	cutoff = abs(20*getMedianChangerate(indata))
	
	b = []
	while len(b)<100: # get the first 100 "sane" lines
		if np.isnan(x):
			pass
		else:
			b.append(x)
	c = np.mean(np.array(b))
	
	if abs(x-c)>cutoff:
		print "\nERROR cannot confirm first value as sane"
		raise Exception
	
	lastKnown,lastIndex = a[0], 0
	for i in range(1,len(a)):
		if np.isnan(a[i]):
			continue
		elif abs(lastKnown-a[i])>(i-lastIndex)*cutoff:
			a[i]=np.nan
		else:
			lastKnown, lastIndex = a[i], i
	
	print "repairing "+ str( len([val for val in a if np.isnan(val)]) ) +" entries"
	 
	lastKnown = a[0]
	for i,x in enumerate(a):
		if np.isnan(x):
			a[i] = lastKnown
		else:
			lastKnown = x	
				
	return np.array(a)
	
def getMedianChangerate(indata, DEBUG=False):
	# this assumes equal spacing of the samples, which is true for temperature and humidity measurements
	a=np.array([]) # contains result
	for i in range(len(indata)-1):
		a = np.append(\
			a, \
			(indata[i+1]-indata[i]) \
		)
	a = np.append(a, np.nan)
	
	# purge NaN for median calculation
	b = np.array([val for val in a if (not np.isnan(val) and val!=0)])
	
	if DEBUG: ## discard test
		c=np.median(b)
		print "# x\tx*c\tdiscard [%]"
		for x in range(1,101):
			print str(x)+"\t"+str(x*c)+"\t"+str(len([val for val in b if (val > x*c)])/float(len(a))*100)
	
	return np.median(b)
	


if __name__=="__main__":

	print "Loading modules"
	import time
	perfstart = time.time()
	import argparse
	
	# set up argparse
	print "Parsing options"
	parser = argparse.ArgumentParser()
	parser.add_argument('-i', dest='infile', action='store', help='specify input file, only FNTCORR files from test02_scan_iv_custom.py are supported!' )
	parser.add_argument('-o', dest='outfile', default='ivt_output_rep.dat', help='specify output file' )
	options = parser.parse_args()

	
	
	outfile = options.outfile
	
	# open file
	infile = p.IVDataObject()
	infile.parseFilename(options.infile) # check before loading data
	if infile.filetype!="FNTCORR":
		print "-i only FNTCORR files are supported!"
		raise Exception

	ftype, fname = p.filestrParse(options.infile)	
	infile.loadFile(fname, ftype) # load data
	
	
	
	# sanitize entries (colums 11,12,13,14)
	columns=[11,12,13,14]
	for c in columns:
		print "sanitizing channel "+str(c)+": ",
		infile.rawData[:,c]=removeFalseTemps(infile.rawData[:,c])
	
	print "Exporting file "+outfile
	fheader = ['# REPAIRED VERSION, original file: '+infile.filename+'\n']
	with open(infile.filename, 'r') as orig:
		oheader = orig.readlines(512*1024) # read the first 512 KB into an array
		for l in oheader:
			if l[0]=='#':
				fheader.append(l)
			else:
				break
	
	with open(outfile, 'w') as f: # overwrites existing file
		f.writelines(fheader)
		np.savetxt(f, infile.rawData)
		
	perfend = time.time()
	print "Time taken: "+str(perfend-perfstart)+"s"
