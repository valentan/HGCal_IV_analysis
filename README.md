# HGCal_IV_analysis



## Installation (on Windows) 

- [Python 2.7](https://www.python.org/) (numpy, matplotlib)
- [HexPlot](https://gitlab.cern.ch/CLICdp/HGCAL/HGCAL_sensor_analysis/tree/master/bin), requires [ROOT](https://root.cern.ch/content/release-53436) for "Windows Visual Studio 2013 (.zip)"
- [PDFtk](https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/)

Add the respective folders to the PATH variable: `\PDFtk\bin\, \HexPlot\, \python27\, \python27\scripts\, \root_v5.34.36.win32.vc12\bin\` and `\HGCal_IV_analysis\`. Make sure to use the folders appropriate for your setup. Restart Windows.



## Usage 

Most of the .py scripts have embedded help, which you can call with the -h argument.

### file naming convention
You can specify the input format with the -i command, like this '-i FORMAT:filename.dat'. 

Supported FORMAT strings are:
- *`FULLNEEDLE` (iv)* probecard measurement
- *`FNTCORR` (ivt)* probecard measurement with extended logging (includes temperature and humidity data)
- *`SEVENNEEDLE` (iv7)* seven-needle measurement
- *`IVPARSER` (ivp)* internal file format exported by some scripts

If you want to use automatic format recognition, you will have to adhere to the naming convention as follows. "Auto format" is required by make_analysis and supported by all other scripts.

`FILETYPE_SENSORID_MEASID.EXT`, e.g. `iv7_VE123456.78_71.txt.gz`
- *`FILETYPE`* iv, ivt, iv7, ivp
- *`SENSORID`* may not contain underscore '_'
- *`MEASID`* may not contain underscore '_'
- *`EXT`* supported extensions are .txt, .dat (ASCII-encoded text files); .txt.gz, .dat.gz (gzip compressed versions)

### example usage
If you run make_analysis.py in a folder with properly named data files (e.g. `ivt_VE628925.04_05.dat`), it will generate output akin to this:

__making split rel.dev. files__   
`ivt_split.py -m reldev -i ivt_VE628925.04_05.dat`

__relative deviations, 6 plots per page__   
`plot16page.py -s 0 -df auto -batch -p 3,2 -xn "voltage:V" -xl '-1000,0' -yn "relative deviation:%" -y2n "reverse current:nA" -y2l '1,4E3' -y2s log -e1 -ps '-1E9' -ps 1 -ps 1 -ps 1 -ps 1 -ps 1 -i ivp_VE628925.04_05_m0reldev.txt -l m0reldev -i ivp_VE628925.04_05_m1reldev.txt -l m1reldev -i ivp_VE628925.04_05_m2reldev.txt -l m2reldev -i ivp_VE628925.04_05_m3reldev.txt -l m3reldev -i ivp_VE628925.04_05_m4reldev.txt -l m4reldev -i ivp_VE628925.04_05_m5reldev.txt -l m5reldev -o VE628925.04_05-reldev.pdf -t "relative deviations, VE628925.04_05"`

__sanitizing temperature values__   
`sanitize_temps.py -i ivt_VE628925.04_05.dat -o ivt_VE628925.04_05_rep.dat`

__correcting for temperature difference__   
`temp_corr.py -i ivt_VE628925.04_05_rep.dat -ft var -tt 22 -o ivp_VE628925.04_05_tcorr.txt`

__HexPlot contact_test__   
`plotHex.py -nowarn -i ivp_VE628925.04_05_tcorr.txt -m contact_test -o VE628925.04_05-contact_test.pdf`

__HexPlot current_development__   
`plotHex.py -nowarn -i ivp_VE628925.04_05_tcorr.txt -m current_development -o VE628925.04_05-current_development.pdf`

__split file by pad type__   
`quadrant_split.py -m type -g "Full;Small edge;Large edge;Mousebite;Small calibration,Large calibration;Guard" -i ivp_VE628925.04_05_tcorr.txt`

__split file of type "Full" by pad region__   
`quadrant_split.py -m region -0 -g "21;40;60;80" -i ivp_VE628925.04_05-t00.txt`

__all IV curves overlay plot, by type and region__   
`plotAll.py -batch -df c -xn "voltage:V" -xl '-1000,0' -yn "reverse current:nA" -ys log -yl 1E1,1E4 -ps '-1E9' -i ivp_VE628925.04_05-t00-r00.txt -l "Full 21u" -i ivp_VE628925.04_05-t00-r01.txt -l "Full 40u" -i ivp_VE628925.04_05-t00-r02.txt -l "Full 60u" -i ivp_VE628925.04_05-t00-r03.txt -l "Full 80u" -i ivp_VE628925.04_05-t00-r04.txt -l "Full other" -i ivp_VE628925.04_05-t01.txt -l "Small edge" -i ivp_VE628925.04_05-t02.txt -l "Large edge" -i ivp_VE628925.04_05-t03.txt -l "Mousebite" -i ivp_VE628925.04_05-t04.txt -l "Calibration" -i ivp_VE628925.04_05-t05.txt -l "Guard" -t "VE628925.04_05" -o VE628925.04_05-ivall.pdf`

__IV details, 16 plots per page__   
`plot16page.py -df auto -batch -xn "voltage:V" -xl '-1000,0' -yn "reverse current:nA" -ys log -ps '-1E9' -i ivp_VE628925.04_05_tcorr.txt -t "IV details, VE628925.04_05" -o VE628925.04_05-ivdetails.pdf`

__run the test for 500V compliance (generates a HexPlot)__   
`test_500v.py -p -lim 8 -i ivp_VE628925.04_05_tcorr.txt`

### plotHex.py
This script contains a file size check, in order to avoid passing large files on to HexPlot.exe. As of the current version, HexPlot will crash if it is given too much data.
Large files will therefore raise an exception during the batch process, which you can safely ignore.
