# ifxgeolib.py

#https://stackoverflow.com/questions/8144026/how-to-define-a-str-method-for-a-class

from numpy import nan as NaN
from math import isnan

class pad:
	def __init__(self, num):
		self.__name__=str(num)
		
		self.num = int(num)
		self.neighbours = []
		return
		
	def __str__(self):
		return "P"+self.__name__
		
	def setNeighbours(self, sides_list, value): # set multiple sides to the same neighbour
		for l in sides_list:
			self.neighbours[l-1]=value
			
	def hasNeighbour(self, nbpad):
		#neighbour = nbpad.num
		for i in self.neighbours:
			if i == None:
				pass
			elif i == nbpad:
				return True # aborts on first occurence
		return False
		
		
class IFX8in:
	def __init__(self):		
		#initialise pads 1 .. 237 to have 6 neighbours
		self.pads = [pad(i) for i in range(1,238+1)]
		for j in self.pads:
			j.neighbours = [None for i in range(6)]
		
		self._seedGuardRing()
		
		# set neighbours along sides 5 and 2
		for k in range(36,47+1):
			self.byID(k).neighbours[2]=self.byID(k+1)
			self.byID(k).neighbours[5]=self.byID(k-1)
		
		return
		
	def byID(self, id):
		for i in self.pads:
			if i.num == int(id):
				return i
		return NaN # TODO this should be an error
		
	def findNeighbours(self, pad):
		result = []
		for i in self.pads:
			if i.hasNeighbour(pad):
				result.append(i)
		return sorted(set(result), key=result.index)
		
	def _seedGuardRing(self):
		pGuardRing = self.byID(238)
		# set guard ring as neighbour (pGuardRing) for edge cells
		for i in [4,11,21,34]: #1
			self.byID(i).setNeighbours([1,2,6], pGuardRing)
		for i in [10,20,33]: #1a
			self.byID(i).setNeighbours([1], pGuardRing)
		for i in [79,110,141,172]: #2
			self.byID(i).setNeighbours([1,2,3], pGuardRing)
		for i in [94,125,156]: #2a
			self.byID(i).setNeighbours([2], pGuardRing)
		for i in [214,224,231,235]: #3
			self.byID(i).setNeighbours([2,3,4], pGuardRing)
		for i in [213,223,230]: #3a
			self.byID(i).setNeighbours([3], pGuardRing)
		for i in [202,215,225,232]: #4
			self.byID(i).setNeighbours([3,4,5], pGuardRing)
		for i in [203,216,226]: #4a
			self.byID(i).setNeighbours([4], pGuardRing)
		for i in [64,95,126,157]: #5
			self.byID(i).setNeighbours([4,5,6], pGuardRing)
		for i in [80,111,142]: #5a
			self.byID(i).setNeighbours([5], pGuardRing)
		for i in [1,5,12,22]: #6
			self.byID(i).setNeighbours([1,5,6], pGuardRing)
		for i in [6,13,23]: #6a
			self.byID(i).setNeighbours([6], pGuardRing)
		
		# set guard ring as neighbour for corner cells
		for i in [2,3]: #6-1
			self.byID(i).setNeighbours([1,6], pGuardRing)
		for i in [48,63]: #1-2
			self.byID(i).setNeighbours([1,2], pGuardRing)
		for i in [187,201]: #2-3
			self.byID(i).setNeighbours([2,3], pGuardRing)
		for i in [233,234]: #3-4
			self.byID(i).setNeighbours([3,4], pGuardRing)
		for i in [173,188]: #4-5
			self.byID(i).setNeighbours([4,5], pGuardRing)
		for i in [35,49]: #5-6
			self.byID(i).setNeighbours([5,6], pGuardRing)
		
		return
		
		

if __name__=="__main__":
	
	x = IFX8in()
	
	# print "List of neighbours: "
	# for i in x.pads:
		# print str(i)+": "+str( [str(j) for j in i.neighbours] )
		
	print "Neighbours of pad 238: "
	temp = x.findNeighbours( x.byID(238) )
	print str( [str(j) for j in temp] )
	
	