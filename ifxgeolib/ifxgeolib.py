# ifxgeolib.py

import numpy as np
from os import path



def elInArr(el, arr): # check if element is in Array
	for i in arr:
		if el==i:
			return True
	return False
	


class pad(object):
	def __init__(self, num):
		self.__name__=str(num)
		
		self.num = int(num)
		self.neighbours = []
		
		self.regions = None
		self.type = None
		return
		
	def __str__(self):
		#https://stackoverflow.com/questions/8144026/how-to-define-a-str-method-for-a-class
		return self.__repr__()
		
	def __repr__(self):
		return "P"+self.__name__
		
		
	def setNeighbours(self, sides_list, value): # set multiple sides to the same neighbour
		for l in sides_list:
			self.neighbours[l-1]=value
			
	def hasNeighbour(self, nbpad):
		#neighbour = nbpad.num
		for i in self.neighbours:
			if i == None:
				pass
			elif i == nbpad:
				return True # aborts on first occurence
		return False
		
	def inGroup(self, group):
		return ( self.inType(group) or self.inRegion(group) )
			
	def inRegion(self, region):
		return elInArr(region, self.regions)
		
	def inType(self, type):
		return ( type == self.type )
		
		
		
class IFX8in(object):
	def __init__(self):		
		#initialise pads 1 .. 237 to have 6 neighbours
		self.pads = [pad(i) for i in range(1,238+1)]
		# for j in self.pads:
			# j.neighbours = [None for i in range(6)]
		
		cDir=path.dirname(path.realpath(__file__)) # directory where this script is located (!= working directory)
		self.parseCSV(cDir+'\IFX8_pad_properties.csv')
		
		return
		
	def byID(self, id):
		for i in self.pads:
			if i.num == int(id):
				return i
		# return NaN # TODO this should be an error
		return None # TODO this should be an error
		
	def findNeighbours(self, pad):
		result = []
		for i in self.pads:
			if i.hasNeighbour(pad):
				result.append(i)
		return sorted(set(result), key=result.index)
		
	# def recFindNeighbours(self, pad_list, recDepthLim=1, recDepth=0):
		# result = []
		# recDepth += 1
		# if recDepth>=recDepthLim:
			# return 
		# for i in self.pads:
			# for j in pad_list:
				# if i.hasNeighbour(j):
					# # result.append(i)
					# result.append( self.recFindNeighbours([j], recDepthLim, recDepth) )
		# return sorted(set(result), key=result.index)
		
	def parseCSV(self, filename):
		with open(filename,'r') as f:
			self.region_list = []
			self.region_list_str = []
			self.type_list = []
			
			for line in f.readlines():
				if line[0]=='#': continue #skip over comment lines
				
				line = line.strip() # remove line ending
				line = line.split(';')
				
				pNum = line[0]
				
				pType = line[1]
				self.byID(pNum).type = pType
				self.type_list.append(pType)
				
				pRegions = line[2].split('-')
				self.byID(pNum).regions = pRegions
				self.region_list.append(pRegions)
				
				temp = str(pRegions[0])
				for k in pRegions[1::]:
					temp+='-'+str(k)
				self.region_list_str.append(temp)
				
				pArea = line[3].replace(',','.') # area is given in mm^2
				self.byID(pNum).area = float(pArea)/1E6 # internally saved in m^2
				
				for j in line[4::]:
					if j!='':
						self.byID(line[0]).neighbours.append(self.byID(j))
			
			self.region_list     = np.unique(np.array(self.region_list))
			self.region_list_str = np.unique(np.array(self.region_list_str))
			self.type_list       = np.unique(np.array(self.type_list))
			
		return
		
		

if __name__=="__main__":
	
	x = IFX8in()
	
	print "PAD\tTYPE\tREGION\tAREA\tNEIGHBOURS"
	for i in x.pads:
		print str(i),
		print str(i.type),
		print str(i.regions),
		print str(i.area),
		print str(i.neighbours)
		
	# print "Neighbours of pad 238: "
	# temp = x.findNeighbours( x.byID(238) )
	# print str( [str(j) for j in temp] )
	
	