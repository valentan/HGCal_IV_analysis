# test_current_at_voltage.py
# example usage: test_current_at_voltage.py -p -nv 500 -lim 8 -i iv7_VE628925.14_70.txt

print "Loading modules"

import time
perfstart = time.time()
import numpy as np
from numpy import dtype
import ivparser as p
from os import system
from os import path
import argparse
import ifxgeolib



def execAndErr(command):
	if system(command)!=0:
		raise Exception
	else:
		return 0

# TODO import this list from a file
wafer_thickness_list = [
# WAFER_SN [-], THICKNESS [m]
	['VE628925.02',200e-6],
	['VE628925.03',200e-6],
	['VE628925.04',300e-6], # w/o TAIKO
	['VE628925.06',350e-6], # w/o TAIKO
	['VE628925.08',200e-6],
	['VE628925.09',200e-6],
	['VE628925.11',140e-6],
	['VE628925.14',200e-6],
	['VE628925.15',200e-6],
	['VE628925.16',300e-6], # w/o TAIKO
	['VE628925.18',350e-6], # w/o TAIKO
	['VE628925.20',200e-6], 
	['VE628925.21',200e-6], 
	['VE628925.23',140e-6], 
	['VE628925.25',350e-6], # w/o TAIKO
	['VE641657.01',200e-6],
	['VE641657.06',200e-6],
	['VE641657.08',200e-6],
	['VE641657.09',200e-6],
	['VE641657.10',200e-6],
	['VE641657.11',200e-6]
]

# set up argparse
print "Parsing options"
parser = argparse.ArgumentParser()
parser.add_argument('-i', dest='infile', action='store', help='specify input file. Automatically sets TYPE according to prefix. Force a TYPE by using TYPE:filename, e.g.\'FULLNEEDLE:iv.dat\'. Supported formats: '+str(p.SETTINGS.keys()) )
parser.add_argument('-o', dest='outfile', action='store', help='specify output filename, script will append .txt and .pdf as needed')
parser.add_argument('-p', dest='hexplot', action='store_true', help='plot the results in HexPlot')
parser.add_argument('-nv', dest='negvoltage', action='store', help='select reverse voltage. Unit: -V')
parser.add_argument('-lim', dest='clim', action='store', help='set current limit: Unit: A/m^3')
parser.add_argument('-th', dest='thickness', action='store', help='override automatic wafer thickness: Unit: m')
options = parser.parse_args()

ftype, fname = p.filestrParse(options.infile)
infile = p.IVDataObject()
infile.loadFile(fname, ftype)

if options.thickness is None:
	foundFlag = False
	for w in wafer_thickness_list:
		if w[0]==infile.sensorID:
			foundFlag = True
			wafer_thickness = w[1]
			break
	
	if not foundFlag:
		print "ERROR sensor not in database (use -th to override)"
	
else:
	wafer_thickness=float(options.thickness)

if options.clim is not None:
	current_limit = float(options.clim)
else:
	print "-lim is a required argument"
	raise Exception
	
if options.negvoltage is not None:
	test_voltage = float(options.negvoltage)
else:
	print "-nv is a required argument"
	raise Exception
	
if options.outfile is None:
	outfile = infile.shortFilename+'-test_'+str(int(test_voltage))+'V'
else:
	outfile = options.outfile
	
ifx = ifxgeolib.IFX8in()

valPASS = 1 # blue in hexplot
valFAIL = 2 # red in hexplot

valTESTID = 1

# current_limit = 8e-9/1e-9 # 8nA/mm^3 = 8A/m^3

fheader = []
fheader.append( '# Testing for current limit\n' )
fheader.append( '# config columns=channel,testID,result,voltage,current\n' )
fheader.append( '# config test_voltage='+str(test_voltage)+'\n' )
fheader.append( '# config current_limit='+str(current_limit)+'\n' )
fheader.append( '# config wafer_thickness='+str(wafer_thickness)+'\n')
fheader.append( '# config valPASS='+str(valPASS)+'\n')
fheader.append( '# config valFAIL='+str(valFAIL)+'\n')
fheader.append( '# \n' )
fheader.append( '# Channel [-]\tTest ID [-]\tNumeric result [1/0]\tVoltage [V]\tCurrent [A]\tCurrent Limit [A]\n' )

fdata = []
#fdata_dtype = [ dtype('int'), dtype('int'), dtype('float'), dtype('float') ]

for ch in infile.channel_list:
	if ifx.byID(ch) is None:
		continue # skip channels that don't represent pads

	chData = p.getChannelData(infile.data, ch)
	
	# regions = ifx.byID(ch).regions
	# type = ifx.byID(ch).type
	chCurrentLimit = ifx.byID(ch).area * wafer_thickness * current_limit

	# find first data entry at or above 500V (reverse voltage)
	vFoundFlag = False
	for v in np.sort(p.getVoltages(chData)*-1):
		if v >= test_voltage:
			vFoundFlag = True
			current = [val[2] for val in chData if val[1]==-v][0]
			
			if abs(current) > abs(chCurrentLimit):
				nres = valFAIL
			else:
				nres = valPASS
			
			fdata.append([int(ch), valTESTID, nres, -v, current, chCurrentLimit])
			
			break # exit voltage loop
		if np.isnan(v):
			break
			
	if not vFoundFlag:
		# fdata.append([int(ch), valTESTID, np.nan, np.nan, np.nan, chCurrentLimit])
		fdata.append([int(ch), valTESTID, valFAIL, np.nan, np.nan, chCurrentLimit])

# numnan = len( [val for val in fdata if np.isnan(val[2])] )
numfailed = len([val for val in fdata if val[2]==valFAIL])
numpassed = len([val for val in fdata if val[2]==valPASS])
pctfailed = float(numfailed)/(numfailed+numpassed)*100

ffooter = []
ffooter.append("# SUMMARY: %i (%.1f%%) of %i have failed the test.\n" % ( numfailed, pctfailed, numfailed+numpassed ) )
# ffooter.append("# SUMMARY: %i could not be evaluated (NaN).\n" % numnan )

if False: # DEBUG output
	for line in fheader:
		print line,
	for line in fdata:
		for c in line:
			print str(c)+'\t',
		print '\n',
	for line in ffooter:
		print line,

with open(outfile+'.txt', 'w') as f: # overwrites existing file
	f.writelines(fheader)
	# fdata = np.array(fdata, dtype=fdata_dtype)
	fdata = np.array(fdata)
	np.savetxt( f, fdata )
	f.writelines(ffooter)
	
if options.hexplot == True:
	print "Making HexPlot"
	cDir=path.dirname(path.realpath(__file__)) # directory where this script is located (!= working directory)
	geofile = cDir+'\hex_positions_IFX_256ch_8inch.txt'
	hexplotformat = 'PADNUM:SELECTOR:VAL'
	infostr = 'lr:TEST~%.1fV~(limit~%.1f~nA/mm3):tr:%s:ll:FAIL~%.1f%%' % ( test_voltage, current_limit, infile.shortFilename, pctfailed )
	execAndErr('HexPlot.exe -g "%s" -o %s --select %i --nd 1 --pn 0 --vn "test result":"pass = %i":"fail = %i" -i %s --if %s -z %i:%i --info %s' % (geofile, outfile+'.pdf', valTESTID, valPASS, valFAIL, outfile+'.txt', hexplotformat, valPASS, valFAIL, infostr ) )
	
perfend = time.time()
print "Time taken: "+str(perfend-perfstart)+"s"
